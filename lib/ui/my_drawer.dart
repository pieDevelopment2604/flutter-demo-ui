import 'package:flutter/material.dart';
import 'package:flutter_ui/app/globals.dart';
import 'package:flutter_ui/providers/home_provider.dart';
import 'package:flutter_ui/widgets/text_widget.dart';
import 'package:provider/provider.dart';

class MyDrawer extends StatelessWidget {
  const MyDrawer({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: Container(
        color: Theme.of(context).primaryColor,
        child: ListView(
          padding: EdgeInsets.zero,
          children: [
            DrawerHeader(
              child: Row(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  const Expanded(
                    child: Padding(
                      padding: EdgeInsets.only(top: 3),
                      child: Texts(
                        "zahid.shaikh.9013@gmail.com",
                        color: Colors.white,
                        fontSize: 16.0,
                      ),
                    ),
                  ),
                  GestureDetector(
                    onTap: () {
                      hideKeyboard(context);
                      Navigator.of(context).pop();
                    },
                    child: const Icon(
                      Icons.close,
                      color: Colors.white,
                    ),
                  )
                ],
              ),
            ),
            ListTile(
              onTap: () {
                context.read<HomeProvider>().setIndex(0);
                hideKeyboard(context);
                Navigator.of(context).pop();
              },
              title: const Texts(
                "Grading",
                color: Colors.white,
                fontSize: 18.0,
              ),
            ),
            ListTile(
              onTap: () {
                context.read<HomeProvider>().setIndex(1);
                hideKeyboard(context);
                Navigator.of(context).pop();
              },
              title: const Texts(
                "Offload Manager",
                color: Colors.white,
                fontSize: 18.0,
              ),
            ),
            ListTile(
              onTap: () {
                context.read<HomeProvider>().setIndex(2);
                hideKeyboard(context);
                Navigator.of(context).pop();
              },
              title: const Texts(
                "Settings",
                color: Colors.white,
                fontSize: 18.0,
              ),
            ),
          ],
        ),
      ),
    );
  }
}
