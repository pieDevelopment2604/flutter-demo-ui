import 'package:flutter/material.dart';
import 'package:flutter_ui/providers/home_provider.dart';
import 'package:flutter_ui/widgets/text_widget.dart';
import 'package:provider/provider.dart';

import 'my_drawer.dart';
import 'widgets/grading_widget.dart';
import 'widgets/offload_widget.dart';
import 'widgets/settings_widget.dart';

class HomeScreen extends StatelessWidget {
  const HomeScreen({Key? key}) : super(key: key);
  static const List<Widget> _pages = [
    GradingWidget(),
    OffloadManagerWidget(),
    SettingsWidget(),
  ];

  @override
  Widget build(BuildContext context) {
    final homeProvider = Provider.of<HomeProvider>(context);
    return Scaffold(
      resizeToAvoidBottomInset: homeProvider.index == 1,
      appBar: AppBar(
        title: Texts(
          homeProvider.title,
          color: Colors.white,
          fontSize: 18.0,
          fontWeight: FontWeight.bold,
        ),
        centerTitle: true,
      ),
      drawer: const MyDrawer(),
      body: IndexedStack(
        index: homeProvider.index,
        children: _pages,
      ),
    );
  }
}
