import 'package:flutter/material.dart';
import 'package:flutter_ui/widgets/text_field_widget.dart';
import 'package:flutter_ui/widgets/text_widget.dart';

class OffloadManagerWidget extends StatefulWidget {
  const OffloadManagerWidget({Key? key}) : super(key: key);

  @override
  _OffloadManagerWidgetState createState() => _OffloadManagerWidgetState();
}

class _OffloadManagerWidgetState extends State<OffloadManagerWidget> {
  final fcHaddock = FocusNode();
  final fcRedfish = FocusNode();
  final tcHaddock = TextEditingController();
  final tcRedfish = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    fcHaddock.dispose();
    fcRedfish.dispose();
    tcHaddock.dispose();
    tcRedfish.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return ListView(
      padding: const EdgeInsets.all(20.0),
      children: [
        const Center(
          child: Texts(
            "Pick your fish",
            fontSize: 18.0,
            fontWeight: FontWeight.bold,
          ),
        ),
        const SizedBox(height: 20),
        const Align(
          alignment: Alignment.centerRight,
          child: Texts(
            "Total Qty",
            fontSize: 18.0,
            fontWeight: FontWeight.bold,
          ),
        ),
        _ItemFish(
          image: "image",
          title: "Haddock",
          focusNode: fcHaddock,
          controller: tcHaddock,
        ),
        const SizedBox(height: 20.0),
        _ItemFish(
          image: "image",
          title: "Redfish",
          focusNode: fcRedfish,
          controller: tcRedfish,
        ),
      ],
    );
  }
}

class _ItemFish extends StatelessWidget {
  final String image;
  final String title;
  final FocusNode focusNode;
  final TextEditingController controller;

  const _ItemFish({
    Key? key,
    required this.image,
    required this.title,
    required this.focusNode,
    required this.controller,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Row(
      children: [
        Container(
          width: 80,
          height: 80,
          decoration: BoxDecoration(
            color: Colors.grey,
            borderRadius: BorderRadius.circular(5),
          ),
        ),
        const SizedBox(width: 16.0),
        Expanded(
          child: Row(
            children: [
              Texts(
                title,
                fontSize: 18.0,
                fontWeight: FontWeight.bold,
              ),
              const SizedBox(width: 30),
              Expanded(
                child: TextFormFieldWidget(
                  focusNode: focusNode,
                  controller: controller,
                  hasDecoration: true,
                ),
              ),
            ],
          ),
        ),
      ],
    );
  }
}
