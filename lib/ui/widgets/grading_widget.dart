import 'package:flutter/material.dart';
import 'package:flutter_ui/widgets/text_field_widget.dart';
import 'package:flutter_ui/widgets/text_widget.dart';

class GradingWidget extends StatefulWidget {
  const GradingWidget({Key? key}) : super(key: key);

  @override
  _GradingWidgetState createState() => _GradingWidgetState();
}

class _GradingWidgetState extends State<GradingWidget> {
  final fcHaddock = FocusNode();
  final tcHaddock = TextEditingController();

  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
    fcHaddock.dispose();
    tcHaddock.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,
      body: Column(
        children: [
          Expanded(
            child: ListView(
              padding: const EdgeInsets.all(20.0),
              children: [
                const Center(
                  child: Texts(
                    "Haddock",
                    fontSize: 18.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
                SizedBox(height: MediaQuery.of(context).size.height * 0.25),
                Padding(
                  padding: const EdgeInsets.symmetric(horizontal: 50),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: [
                      const Texts(
                        "Weight",
                        fontSize: 18.0,
                        fontWeight: FontWeight.bold,
                      ),
                      const SizedBox(height: 15),
                      TextFormFieldWidget(
                        focusNode: fcHaddock,
                        controller: tcHaddock,
                        hasDecoration: true,
                      ),
                    ],
                  ),
                ),
              ],
            ),
          ),
          Padding(
            padding: const EdgeInsets.all(8.0),
            child: ElevatedButton(
              onPressed: () {},
              style: ElevatedButton.styleFrom(
                fixedSize: const Size(200, 45),
              ),
              child: const Texts(
                "Add to Total",
                fontSize: 18.0,
                color: Colors.white,
                fontWeight: FontWeight.bold,
              ),
            ),
          ),
        ],
      ),
    );
  }
}
