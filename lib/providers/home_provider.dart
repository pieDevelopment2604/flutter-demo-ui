import 'package:flutter/foundation.dart';

class HomeProvider extends ChangeNotifier {
  String _title = "Settings";

  String get title => _title;

  /// set default to settings
  int _index = 2;

  int get index => _index;

  void setIndex(int index) {
    if (_index == index) return;
    _index = index;

    if (_index == 0) {
      _title = "Grading";
    } else if (_index == 1) {
      _title = "Offload";
    } else {
      _title = "Settings";
    }
    notifyListeners();
  }
}
