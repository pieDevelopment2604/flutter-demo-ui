import 'package:colours/colours.dart';
import 'package:flutter/material.dart';

class Palette {
  static const Color accentColor = Color(0xff124D3D);

  static const orange = Color(0xFFff7d24);
  static const red = Color(0xFFFF0000);
  static const green = Color(0xFF00B14C);

  static const grey = Colors.grey;
  static const black = Colors.black;
  static const white = Colors.white;

  static final appTheme = ThemeData(
    primarySwatch: Colours.swatch("#124D3D"),
    backgroundColor: Colors.white,
  );
}
