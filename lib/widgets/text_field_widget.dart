import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_ui/app/theme/palette.dart';

class TextFormFieldWidget extends StatefulWidget {
  final TextInputType? textInputType;
  final String? hintText;
  final TextDirection? hintTextDirection;
  final TextStyle? hintStyle;
  final Widget? prefixIcon;
  final Widget? suffixIcon;
  final String? defaultText;
  final String? errorText;
  final FocusNode? focusNode;
  final bool? isObscure;
  final TextEditingController controller;
  final FormFieldValidator<String>? functionValidate;
  final String? parametersValidate;
  final TextInputAction? actionKeyboard;
  final Function(String?)? onSaved;
  final Function? onFieldSubmitted;
  final Function? onFieldTap;
  final Function(String)? onTextChanged;
  final String? label;
  final String? richS1;
  final String? richS2;
  final bool? hasDecoration;
  final int? minLines;
  final int? maxLines;
  final bool readOnly;
  final bool? enabled;
  final bool? autoFocus;
  final bool? filled;
  final Color? filledColor;
  final bool? hasFocus;
  final bool? isRequired;
  final String? obscureSymbol;
  final InputBorder? border;
  final InputBorder? errorBorder;
  final InputBorder? focusedBorder;
  final InputBorder? focusedErrorBorder;
  final InputBorder? enabledBorder;
  final InputBorder? disabledBorder;
  final bool? wantListeners;
  final Function(bool)? onFocusChange;
  final List<TextInputFormatter>? inputFormatters;
  final int? maxLength;
  final bool? showCounter;
  final EdgeInsetsGeometry? contentPadding;
  final TextAlign? textAlign;
  final TextCapitalization textCapitalization;

  const TextFormFieldWidget({
    required this.controller,
    this.hintText,
    this.hintTextDirection,
    this.hintStyle,
    this.focusNode,
    this.textInputType,
    this.defaultText,
    this.errorText,
    this.isObscure = false,
    this.functionValidate,
    this.parametersValidate,
    this.actionKeyboard = TextInputAction.next,
    this.onSaved,
    this.onFieldSubmitted,
    this.onFieldTap,
    this.onTextChanged,
    this.prefixIcon,
    this.suffixIcon,
    this.label,
    this.richS1 = '',
    this.richS2 = '',
    this.hasDecoration = false,
    this.minLines = 1,
    this.maxLines = 1,
    this.readOnly = false,
    this.enabled = true,
    this.autoFocus = false,
    this.filled = false,
    this.filledColor = Colors.white,
    this.hasFocus = false,
    this.isRequired = false,
    this.obscureSymbol,
    this.border,
    this.errorBorder,
    this.focusedBorder,
    this.focusedErrorBorder,
    this.enabledBorder,
    this.disabledBorder,
    this.wantListeners = false,
    this.onFocusChange,
    this.inputFormatters,
    this.maxLength,
    this.showCounter,
    this.contentPadding = const EdgeInsets.all(15.0),
    this.textAlign,
    this.textCapitalization = TextCapitalization.none,
  });

  @override
  TextFormFieldWidgetState createState() => TextFormFieldWidgetState();
}

class TextFormFieldWidgetState extends State<TextFormFieldWidget> {
  @override
  void initState() {
    super.initState();

    if (widget.wantListeners == true) {
      widget.focusNode?.addListener(() {
        widget.onFocusChange?.call(widget.focusNode!.hasFocus);
      });
    }
  }

  static customTextStyle({
    Color color = Colors.black,
    FontWeight fontWeight = FontWeight.normal,
    double fontSize = 15.0,
    TextDecoration? textDecoration = TextDecoration.none,
    double? letterSpacing,
    String? fontFamily,
    var shadows,
    FontStyle? fontStyle,
  }) {
    return TextStyle(
      color: color,
      fontWeight: fontWeight,
      fontSize: fontSize,
      fontFamily: fontFamily,
      decoration: textDecoration,
      letterSpacing: letterSpacing,
      shadows: shadows,
      fontStyle: fontStyle,
    );
  }

  @override
  Widget build(BuildContext context) {
    return Theme(
      data: Theme.of(context).copyWith(
        primaryColor: Colors.black,
        textSelectionTheme: TextSelectionThemeData(
          selectionColor: Colors.grey[300],
        ),
      ),
      child: TextFormField(
        key: widget.key,
        // cursorColor: hasFocus! ? Palette.accentColor : Palette.white,
        cursorColor: Palette.black,
        obscureText: widget.isObscure!,
        keyboardType: widget.textInputType,
        textInputAction: widget.actionKeyboard,
        focusNode: widget.focusNode,
        readOnly: widget.readOnly,
        enabled: widget.enabled,
        autofocus: widget.autoFocus!,
        textCapitalization: widget.textCapitalization,
        // obscuringCharacter: obscureSymbol!,
        style: customTextStyle(
          color: Palette.black,
          fontSize: 15.0,
        ),
        strutStyle: const StrutStyle(
          fontSize: 15.0,
          forceStrutHeight: true,
        ),
        initialValue: widget.defaultText,
        onChanged: widget.onTextChanged,
        minLines: widget.minLines,
        maxLines: widget.maxLines,
        textAlign: widget.textAlign ?? TextAlign.start,
        buildCounter: (BuildContext context, {int? currentLength, int? maxLength, bool? isFocused}) => null,
        decoration: InputDecoration(
          suffixIcon: widget.suffixIcon,
          prefixIcon: widget.prefixIcon,
          filled: widget.filled,
          fillColor: widget.filled! ? widget.filledColor : null,
          hintText: widget.hintText,
          hintTextDirection: widget.hintTextDirection,
          hintStyle: widget.hintStyle ?? customTextStyle(color: Palette.grey, fontSize: 12.0),
          border: widget.hasDecoration!
              ? widget.border ?? const OutlineInputBorder(borderSide: BorderSide(color: Palette.grey))
              : InputBorder.none,
          errorBorder: widget.hasDecoration!
              ? widget.errorBorder ?? const OutlineInputBorder(borderSide: BorderSide(color: Palette.red))
              : InputBorder.none,
          focusedBorder: widget.hasDecoration!
              ? widget.focusedBorder ??
                  const OutlineInputBorder(borderSide: BorderSide(color: Palette.accentColor))
              : InputBorder.none,
          focusedErrorBorder: widget.hasDecoration!
              ? widget.focusedErrorBorder ??
                  const OutlineInputBorder(borderSide: BorderSide(color: Palette.red))
              : InputBorder.none,
          enabledBorder: widget.hasDecoration!
              ? widget.enabledBorder ??
                  const OutlineInputBorder(borderSide: BorderSide(color: Palette.grey, width: 0.0))
              : InputBorder.none,
          disabledBorder: widget.hasDecoration!
              ? widget.disabledBorder ??
                  const OutlineInputBorder(borderSide: BorderSide(color: Colors.white, width: 0.0))
              : InputBorder.none,
          contentPadding: widget.contentPadding,
          isDense: true,
          errorText: widget.errorText,
          errorMaxLines: 2,
          errorStyle: customTextStyle(
            color: Palette.red,
            fontSize: 12.0,
            letterSpacing: 1.2,
          ),
        ),
        controller: widget.controller,
        validator: widget.functionValidate,
        maxLength: widget.maxLength,
        maxLengthEnforcement: MaxLengthEnforcement.enforced,
        inputFormatters: widget.inputFormatters,
        onSaved: (value) {
          if (widget.onSaved != null) widget.onSaved?.call(value);
        },
        onFieldSubmitted: (value) {
          if (widget.onFieldSubmitted != null) widget.onFieldSubmitted?.call();
        },
        onTap: () {
          if (widget.onFieldTap != null) widget.onFieldTap?.call();
        },
      ),
    );
  }
}

void changeFocus(BuildContext context, FocusNode currentFocus, FocusNode nextFocus) {
  currentFocus.unfocus();
  FocusScope.of(context).requestFocus(nextFocus);
}

class EnsureVisible extends StatefulWidget {
  final Widget child;

  const EnsureVisible({
    Key? key,
    required this.child,
  }) : super(key: key);

  @override
  _EnsureVisibleState createState() => _EnsureVisibleState();
}

class _EnsureVisibleState extends State<EnsureVisible> with WidgetsBindingObserver {
  @override
  void initState() {
    super.initState();
    WidgetsBinding.instance!.addObserver(this);
    WidgetsBinding.instance!.focusManager.addListener(autoScroll);
  }

  @override
  void dispose() {
    WidgetsBinding.instance!.removeObserver(this);
    WidgetsBinding.instance!.focusManager.removeListener(autoScroll);
    super.dispose();
  }

  @override
  void didChangeMetrics() {
    autoScroll();
  }

  @override
  Widget build(BuildContext context) => widget.child;

  void autoScroll() async {
    var focussedNode = WidgetsBinding.instance!.focusManager.primaryFocus;
    if (focussedNode != null) {
      var v = focussedNode.context?.findAncestorWidgetOfExactType<EnsureVisible>();
      if (v == widget) {
        // Flutter ALSO tries to adjust the focus / scroll
        await Future.delayed(const Duration(milliseconds: 50));
        // Make sure it's not past the bottom
        Scrollable.ensureVisible(
          context,
          alignmentPolicy: ScrollPositionAlignmentPolicy.keepVisibleAtEnd,
        );
        // // ...nor the top
        Scrollable.ensureVisible(
          context,
          alignmentPolicy: ScrollPositionAlignmentPolicy.keepVisibleAtStart,
        );
      }
    }
  }
}
